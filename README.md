# c3d

Too lazy to even right-click -> "Save file as..."

---

Binary releases for a plethora of OSes and architectures [here](https://gitlab.com/gsora/c3d/-/tags).

## Usage:

Pipe in/pass a parameter pointing to file containing a newline-divided set of [media.ccc.de](httpsmedia.ccc.de) URLs,
direct video URLs will be printed on stdout.

Download example:

```shell script
for i in $(./c3d list.txt); do
  echo "Downloading $i..."
  curl -O -L $i
end
```