package main

import (
	"bufio"
	"flag"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"io"
	"log"
	"net/http"
	"os"
)

func main() {
	stat, _ := os.Stdin.Stat()
	if (stat.Mode() & os.ModeCharDevice) == 0 {
		scan(os.Stdin)
		return
	}

	flag.Parse()
	filePath := flag.Arg(0)

	if filePath == "" {
		log.Println("pass a newline-divided file containing media.ccc.de files, or pipe some urls")
		return
	}

	f, err := os.Open(filePath)
	if err != nil {
		log.Fatal(err)
	}

	scan(f)

}

func scan(r io.Reader) {
	scanner := bufio.NewScanner(r)

	for {
		end := scanner.Scan()
		if !end {
			return
		}
		getVideoURL(scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func getVideoURL(pageURL string) {
	data, err := http.Get(pageURL)
	if err != nil {
		log.Fatal(err)
	}
	defer data.Body.Close()

	doc, err := goquery.NewDocumentFromReader(data.Body)
	if err != nil {
		log.Fatal(err)
	}

	doc.Find("meta").Each(func(i int, selection *goquery.Selection) {
		attr, found := selection.Attr("property")
		if !found {
			return
		}

		if attr == "og:video" {
			videoURL, found := selection.Attr("content")
			if !found {
				log.Println("og:video tag found, but no content was attached: " + pageURL)
			}

			fmt.Println(videoURL)
		}
	})
}
